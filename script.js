const root = document.getElementById("root");

function renderAsPage(html) {
  root.innerHTML = html;
}

const pageHTML = `
<div id="content">
    <div id="controls">
        <select id="sorting">
            <option value="episodes">Numder of episodes</option>
            <option value="dateDown">Date &#8595;</option>
            <option value="dateUp">Date &#8593;</option>
        </select>
        <input id="search" placeholder="try search...">
    </div>
    <div id="characters"></div>
    <div id="load-more">
</div>`;

const loadingHTML = "<h1>Loading...</h1>";

renderAsPage(loadingHTML);

class CharactersHTML {
  constructor(characters) {
    this.characters = characters;
    this.episodes = [];
    this.offset = 0;
    this.limit = 10;
    this.sortBy = "dateDown";
    this.searchValue = "";
    this.searchResult = [];
    this.useSearch = false;
  }

  async loadEpisodes() {
    let page = 1;
    let data = null;
    do {
      let res = await fetch(
        "https://rickandmortyapi.com/api/episode?page=" + page
      );
      data = await res.json();
      this.episodes.push(...data.results);
      page++;
    } while (data.info.next !== null);
  }

  initPage() {
    renderAsPage(pageHTML);
    let sortingField = document.getElementById("sorting");
    sortingField.value = this.sortBy;
    sortingField.addEventListener("change", (element) => {
      this.sortBy = sortingField.value;
      this.sort();
      this.refresh();
    });

    let seachField = document.getElementById("search");
    seachField.addEventListener("input", (element) => {
      if (seachField.value === "") {
        this.searchResult = [];
        this.useSearch = false;
        this.searchValue = seachField.value;
      } else {
        this.useSearch = true;
      }
      this.searchValue = seachField.value;
      this.updateSearch(seachField.value);
      this.refresh();
      seachField = document.getElementById("search");
      seachField.focus();
    });
    seachField.value = this.searchValue;
  }

  updateSearch(searchString) {
    this.searchResult = this.characters.filter((c) => {
      return c.name.toLowerCase().includes(searchString.toLowerCase());
    });
    this.offset = 0;
  }

  refresh() {
    this.initPage();
    this.load();
  }

  sort() {
    if (this.sortBy == "dateDown") {
      this.characters.sort((a, b) => {
        return new Date(a.created) - new Date(b.created);
      });
    } else if (this.sortBy == "dateUp") {
      this.characters.sort((a, b) => {
        return new Date(b.created) - new Date(a.created);
      });
    } else if (this.sortBy == "episodes") {
      // Sort doen't work correct after date dateUp =(
      this.characters.sort((a, b) => {
        let episodeAmountA = a.episode.length;
        let episodeAmountB = b.episode.length;
        if (episodeAmountA == episodeAmountB) {
          return new Date(a.created) - new Date(b.created);
        }
        return a - b;
      });
    }
  }

  getEpisodeTitleById(episdoUrl) {
    let urlParts = episdoUrl.split("/");
    let episodeNumber = urlParts[urlParts.length - 1];
    let episodeIndex = episodeNumber - 1;
    return this.episodes[episodeIndex].name;
  }

  insertCharacter(character, arrayIndex) {
    let charactersElement = document.getElementById("characters");
    let child = document.createElement("div");
    child.className = "character";

    let episodesTitle = character.episode.map((url) => {
      return this.getEpisodeTitleById(url);
    });

    let buttonId = character.name + arrayIndex;
    let buttonHTML = `<button class="remove-button" id="${buttonId}" data-id="${arrayIndex}">Remove</button>`;

    // removing doesn't work correct when search applied
    if (this.useSearch) {
      buttonHTML = `<button class="remove-button" id="${buttonId}" data-id="${arrayIndex}" disabled>Remove</button>`;
    }

    child.innerHTML = `
            <div class="title-container">
              <div class="title-container-element">
                <img class="image" src="${character.image}" />   
              </div>    
              <div class ="title-container-element information">
                <p class="name">${character.name}</p>
                <ul>
                  <li><p class ="race">${character.species}</p></li>
                  <li><p class="location">${character.location.name}</p></li>
                  <li><p class="created">${character.created}</p></li>
                </ul>
              </div>
            </div>
            <div class="episodes">
              <h3>EPISODES:</h3>
              <p class="episodes-text">${episodesTitle.join(" | ")}</p>
            </div>
            ${buttonHTML}
        `;
    charactersElement.appendChild(child);
    let removeButton = document.getElementById(buttonId);
    removeButton.addEventListener("click", (event) => {
      console.log("ss");
      this.characters.splice(event.srcElement.dataset.id, 1);
      this.refresh();
    });
  }

  load() {
    let loopOver = this.characters;
    if (this.useSearch) {
      loopOver = this.searchResult;
    }

    let minOffsetOrLenght = Math.min(this.offset + this.limit, loopOver.length);
    for (let i = 0; i < minOffsetOrLenght; i++) {
      this.insertCharacter(loopOver[i], i);
    }

    let charactersElement = document.getElementById("characters");

    if (minOffsetOrLenght < loopOver.length) {
      let button = document.createElement("button");
      button.innerText = "Load more";
      button.addEventListener("click", () => {
        this.offset += this.limit;
        this.refresh();
        charactersElement.removeChild(button);
      });
      charactersElement.append(button);
    } else if (this.offset > 0 && loopOver.length > this.limit) {
      let button = document.createElement("button");
      button.innerText = "Show less";
      button.addEventListener("click", () => {
        this.offset = 0;
        this.refresh();
      });
      charactersElement.append(button);
    }
  }
}

const url = "https://rickandmortyapi.com/api/character";
fetch(url)
  .then((res) => res.json())
  .then(async (data) => {
    const rawData = data.results;
    console.log(rawData);

    let charactersHTML = new CharactersHTML(rawData);
    await charactersHTML.loadEpisodes();
    charactersHTML.refresh();
  })
  .catch((error) => {
    console.log(error);
    console.log(JSON.stringify(error));
  });
